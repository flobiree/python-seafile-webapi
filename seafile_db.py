# -*- coding: utf-8 -*-
###############################################################################
#       seafile_db.py
#       
#       Copyright © 2023-2025, Florence Birée <florence@biree.name>
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafile database manipulation library (part of Seafile web API)

This library is made for auth backend migration, starting from seafile v11.
Seafile doc: https://manual.seafile.com/deploy/auth_switch/
"""

__author__ = "Florence Birée"
__version__ = "0.12"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023-2025, Florence Birée <florence@biree.name>"

import os
import pymysql
pymysql.install_as_MySQLdb()
from datetime import datetime

class SeafileDB:
    """The Seafile DB manipulation class"""
    
    def __init__(self, ccnet_db_name, seahub_db_name, username, password, host="127.0.0.1", port=3306):
        """Initialize the connexion to the Seahub database"""
        self.ccnet_db_name = ccnet_db_name
        self.seahub_db_name = seahub_db_name
        self.db_user = username
        self.db_password = password
        self.db_host = host
        self.db_port = port
        
        # connect to ccnet_db
        self.ccnet_conn = pymysql.connect(
            user=self.db_user,
            host=self.db_host,
            port=self.db_port,
            password=self.db_password,
            database=self.ccnet_db_name
        )
        self.ccnet_conn.autocommit(True)
        self.ccnet_cursor = self.ccnet_conn.cursor()
        
        # connect to seahub_db
        self.seahub_conn = pymysql.connect(
            user=self.db_user,
            host=self.db_host,
            port=self.db_port,
            password=self.db_password,
            database=self.seahub_db_name
        )
        self.seahub_conn.autocommit(True)
        self.seahub_cursor = self.seahub_conn.cursor() 
    
    def close(self):
        """Close database connexion"""
        self.ccnet_cursor.close()
        self.ccnet_conn.close()
        self.seahub_cursor.close()
        self.seahub_conn.close()

    def auth_info(self, mail_id):
        """Return authentification info for `mail_id`
        
            Return:
            {
                mail_id: "",
                password_hash: "",
                provider: "", # None if no SSO
                provider_uid: "", # SSO UID, None if no SSO
                extra_data: "", # SSO extra_data, None if no SSO
            }
            or None if mail_id not found
        """
        sql_cmd = "SELECT email,left(passwd,25) FROM EmailUser WHERE email = '{}'"
        self.ccnet_cursor.execute(sql_cmd.format(mail_id))
        try:
            (mail_id, password_hash)= self.ccnet_cursor.fetchone() # ('id@auie', '!')
        except TypeError:
            return None # not found
        
        # load SSO data
        sql_cmd = "SELECT username,provider,uid,extra_data FROM social_auth_usersocialauth WHERE username = '{}'"
        self.seahub_cursor.execute(sql_cmd.format(mail_id))
        res_number = self.seahub_cursor.rowcount
        
        if res_number == 1:
            (mail_id, provider, provider_uid, extra) = self.seahub_cursor.fetchone() # ('id@aui', 'ldap', 'mail@id.ldap', '')
            return {
                'mail_id': mail_id,
                'password_hash': password_hash,
                'provider': provider,
                'provider_uid': provider_uid,
                'extra_data': extra,
            }
        else:
            return {
                'mail_id': mail_id,
                'password_hash': password_hash,
                'provider': None,
                'provider_uid': None,
                'extra_data': None,
            }
        
    def get_seaf_id_by_sso(self, provider, provider_uid):
        """Return the seafile `mail_id` given the SSO provider and SSO uid"""
        sql_cmd = "SELECT username FROM social_auth_usersocialauth WHERE provider = '{}' AND uid = '{}'"
        self.seahub_cursor.execute(sql_cmd.format(provider, provider_uid))
        result = self.seahub_cursor.fetchone()
        if result:
            return result[0]
        else:
            return None

    def remove_force_password_change(self, mail_id):
        """Remove the option to force password change (set after an user
        creation)
        
        Use this if you create a local user, and then you want to switch it to
        a SSO
        """
        sql_cmd = "DELETE FROM options_useroptions WHERE email = '{}' AND option_key='force_passwd_change'"
        self.seahub_cursor.execute(sql_cmd.format(mail_id))

    def change_auth(self, mail_id, provider=None, provider_uid=None):
        """Change the SSO for `mail_id`:
            - to native authentification if `provider` is None
            - to the `provider` SSO with `provider_uid` else
              (this can also be used to changed uid for the same provider)
            
            If the new authentification system is native, you must manually
            reset the user password after this operation.
        """
        # check actual status
        current_status = self.auth_info(mail_id)
        if not current_status:
            # User not found, aboring
            return

        # if future_provider is none
        if provider is None:
            # remove field in social_auth_usersocialauth
            sql_cmd = "DELETE FROM social_auth_usersocialauth WHERE username = '{}'"
            self.seahub_cursor.execute(sql_cmd.format(mail_id))
            #result = self.seahub_cursor.fetchone()
            # TODO : reset password
        else:
            # if actual provider is none
            if current_status['provider'] is None:
                # set ! in password
                sql_cmd = "UPDATE EmailUser SET passwd = '!' WHERE email = '{}'"
                self.ccnet_cursor.execute(sql_cmd.format(mail_id))                       
                #(mail_id, password_hash)= self.ccnet_cursor.fetchone() # ('id@auie', '!')
                # add line in social_auth_usersocialauth
                sql_cmd = "INSERT INTO `social_auth_usersocialauth` (`username`, `provider`, `uid`, `extra_data`) VALUES ('{}', '{}', '{}', '')"
                self.seahub_cursor.execute(sql_cmd.format(mail_id, provider, provider_uid))
            else:
                # update line in social_auth_usersocialauth
                sql_cmd = "UPDATE `social_auth_usersocialauth` SET `provider`='{}', `uid`='{}', `extra_data`='' WHERE `username`='{}'"
                self.seahub_cursor.execute(sql_cmd.format(provider, provider_uid, mail_id))
