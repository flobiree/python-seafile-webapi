# Seafile web API python module

This python module is a wrapper to the Seafile web API.

## Dependencies

python-requests
pymysql

## Usage

    from seafile_webapi import SeafileClient
    
    client = SeafileClient("https://seafile.example/")
    # first time authentication, with password
    client.authenticate("user@email.com", password="secret")
    token = client.token
    # other times, authentication with token
    client.authenticate("user@email.com", token=token)
    
    # for all available functions, see:
    help(SeafileClient)

## Exemple script with shell

You can see `seafsh.py` as an sample program which use seafile_webapi. It's
a shell to navigate through your seafile libraries.

## Oauth compatible usage

To use the API with oauth, or another SSO in Seafile, you must set
ENABLE_GET_AUTH_TOKEN_BY_SESSION = True
in your seahub_settings.py

Since v0.11, with this option activated, you can authenticate by retriving a
web API session token from cookies issued by a successful seafile login
(either regular or social login). This can be used by an app running on the
same domain than Seafile, an so able to access to the Seafile cookies.
